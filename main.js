import express from 'express';
import validUrl from 'valid-url';
import axios from 'axios';
import sharp from 'sharp';
import { encode } from 'blurhash';

const app = express();
const port = parseInt(process.env.PORT) || 4000;

const maxWidth = parseInt(process.env.MAX_WIDTH) || 64;
const maxHeight = parseInt(process.env.MAX_HEIGHT) || 64;

const componentX = parseInt(process.env.COMPONENT_X) || 5;
const componentY = parseInt(process.env.COMPONENT_Y) || 5;

app.get('/', async (req, res) => {
    const url = req.query['image-url'];

    if (!url) {
        error(res, 400, 'Query parameter `image-url` required!');
    } else {
        try {
            const testedUrl = validUrl.isWebUri(url);

            if (testedUrl) {
                const response = await computeBlurHashFromImageUrl(testedUrl, maxWidth, maxHeight).catch(err => {
                    error(res, 500, err);
                });

                successResponse(res, response);
            } else {
                error(res, 400, 'The `image-url` string must be valid URL!');
            }
        } catch (err) {
            error(res, 500, err);
        }
    }
});

app.listen(port, () => {
    console.log(`Service started successfully on port ${port}.\nmaxWidth = ${maxWidth};\nmaxHeight = ${maxHeight};\ncomponentX = ${componentX};\ncomponentY = ${componentY};`);
});

function successResponse(res, msg) {
    const response = {
        status: 'ok',
        result: msg
    };

    res.status(200).json(response);

}

function error(res, status, msg) {
    const err = new Error();

    err.status = 'error';
    err.message = msg;

    res.status(status).json(err);
}

async function computeBlurHashFromImageUrl(url, maxWidth, maxHeight) {
    return await axios.get(url, { responseType: 'arraybuffer' })
        .then(async (response) => {
            const { data: pixels, info: metadata } = await sharp(response.data).resize(maxWidth, maxHeight, { fit: 'inside' }).ensureAlpha().raw().toBuffer({ resolveWithObject: true });

            const clamped = new Uint8ClampedArray(pixels);

            const { cX, cY } = getOptimalComponents(metadata.width, metadata.height, componentX, componentY);

            const hash = encode(
                clamped,
                metadata.width,
                metadata.height,
                cX,
                cY
            );

            return {
                blurhash: hash,
                width: metadata.width,
                height: metadata.height,
                componentX: cX,
                componentY: cY
            }
        })
        .catch((err) => {
            console.log(`Couldn't process: ${err}`);
        });
}

/**
 * Get the optimal componentX and componentY based on the aspect ratio of the image.
 * 
 * @param {int} originalWidth Original width of the image.
 * @param {int} originalHeight Original width of the image.
 * @param {int} maxComponentX Maximum value for componentX.
 * @param {int} maxComponentY Maximum value for componentY.
 * 
 * @returns {object} {cX: optimalComponentX, cY: optimalComponentY}
 * 
 * @example getOptimalComponents(1920, 1080, 5, 5) => {cX: 5, cY: 3}
 * @example getOptimalComponents(180, 1920, 5, 5) => {cX: 3, cY: 5}
 * @example getOptimalComponents(1920, 1080, 5, 5) => {cX: 5, cY: 4}
 */
function getOptimalComponents(originalWidth, originalHeight, maxComponentX, maxComponentY){
    if (maxComponentX >= originalWidth && maxComponentY >= originalHeight){
        return(maxComponentX, maxComponentY);
    }

    let optimalComponentX = maxComponentX;
    let optimalComponentY = maxComponentY;

    const originalAspectRatio = originalWidth / originalHeight;

    const targetAspectRatio = maxComponentX / maxComponentY;

    if (originalAspectRatio <= 1){
        optimalComponentX = maxComponentY * originalAspectRatio;
    } else {
        if (originalAspectRatio > targetAspectRatio) {
            optimalComponentY = maxComponentX / originalAspectRatio;
        } else {
            optimalComponentX = maxComponentY / targetAspectRatio;
        }
    }

    return {
        cX: Math.round(optimalComponentX),
        cY: Math.round(optimalComponentY)
    };
}
