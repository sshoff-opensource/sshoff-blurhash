# SSHOFF BlurHash

Service for creating BlurHash by an image URL.

## Environment variables

### Express

* `PORT` - listening port. Default is `4000`.

### BlurHash

Before calculating a blurhash an image should be resized for speed purposes of the hash algorithm.

* `MAX_WIDTH` - maximum **width** of an image that will be hashed. Default is `64`.
* `MAX_HEIGHT` - maximum **height** of an image that will be hashed. Default is `64`.

From the [official documentation](https://github.com/woltapp/blurhash):

> The more components you pick, the more information is retained in the placeholder, but the longer the BlurHash string will be. Also, it doesn't always look good with too many components. We usually go with 4 by 3, which seems to strike a nice balance.
>
> However, you should adjust the number of components depending on the aspect ratio of your images. For instance, very wide images should have more X components and fewer Y components.*

* `COMPONENT_X`. Default is `4`.
* `COMPONENT_Y`. Default is `3`.

`COMPONENT_X` and `COMPONENT_Y` will be changed place if **height** of an image is more than **width**.

## Examples

Example of a request:

`https://site.com/?image-url=https%3A%2F%2Ftest-bucket.s3.us-east-2.amazonaws.com%2Fimages%2Fimage.jpeg`

### Successful response

HTTP code: `200`.

Example of a body:

```JSON
{
    "status": "ok",
    "result": {
        "blurhash": "LXK]+pWmI:og=}s:oLoL}skDj?WB",
        "width": 64,
        "height": 43,
        "componentX": 5,
        "componentY": 3
    }
}
```

### Response with error

HTTP code: **not 200**.

Example of a body:
```JSON
{
    "status": "error",
    "message": "Text of the error"
}
```

## License

[MIT](https://en.wikipedia.org/wiki/MIT_License).